package practica2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/


/**
 *
 * @author Cristobal
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Mapa extends JPanel  {
    
    private final int WIDTH;
    private final int HEIGHT;
    private BufferedImage canvas;
    private JScrollPane panel;
    public ArrayList<int[]> recorrido;
    
    public Mapa() {
        WIDTH = HEIGHT = 10;
        recorrido=new ArrayList<int[]>();
        canvas = new BufferedImage(5000, 5000, BufferedImage.TYPE_INT_ARGB);
        fillCanvas(Color.GRAY);
        
    }
    public Mapa(int anchura, int altura,int pAnchura, int pAltura) {
        recorrido=new ArrayList<int[]>();
        WIDTH = pAnchura;
        HEIGHT = pAltura;
        canvas = new BufferedImage(anchura, altura, BufferedImage.TYPE_INT_ARGB);
        fillCanvas(Color.GRAY);
        
    }
    

    public void fillCanvas(Color c) {
        int color = c.getRGB();
        for (int x = 0; x < canvas.getWidth(); x++) {
            for (int y = 0; y < canvas.getHeight(); y++) {
                canvas.setRGB(x, y, color);
            }
        }
        repaint();
    }

   
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(canvas, null, null);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(canvas.getWidth(), canvas.getHeight());
    }
    
    public void drawLine(Color c, int x1, int y1, int x2, int y2) {
        // Implement line drawing
        repaint();
    }

    public void drawRect(Color c, int x1, int y1, int width, int height) {
        int color = c.getRGB();
        // Implement rectangle drawing
        for (int x = x1; x < x1 + width; x++) {
            for (int y = y1; y < y1 + height; y++) {
                canvas.setRGB(x, y, color);
            }
        }
        repaint();
    }

    
    public void set(int i, int j, int tipo){
       
        switch(tipo){
            case 1 :drawRect(Color.BLACK, i, j, WIDTH, HEIGHT);
            case 2 :drawRect(Color.GREEN, i, j, WIDTH, HEIGHT);
            case 0 :drawRect(Color.WHITE, i, j, WIDTH, HEIGHT);
        }
        repaint();
        
    }
        public void setValor(int i, int j, int tipo){
       
        switch(tipo){
            case 1 :drawRect(Color.black, i*WIDTH, j*HEIGHT, WIDTH, HEIGHT);
                break;
            case 2 :drawRect(Color.RED, i*WIDTH, j*HEIGHT, WIDTH, HEIGHT);
                break;
            case 3 :drawRect(Color.blue, i*WIDTH, j*HEIGHT, WIDTH, HEIGHT);
                break;
            case 4 :drawRect(Color.red,  i*WIDTH, j*HEIGHT, WIDTH, HEIGHT);
                break;
            case 0 :drawRect(Color.white,  i*WIDTH, j*HEIGHT, WIDTH, HEIGHT);
                break;
            
        }
        repaint();
        
    }
    //Con esta funcion se dibuja el recorrido
    public void pintarRecorrido(){
        int tam=recorrido.size();
        int i=0;
        for (int[] next : recorrido) {
            drawRect(new Color(0, 255*i/tam, 150), next[0]*10, next[1]*10, WIDTH, HEIGHT);
            i++;
        }
    }
}

