/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import es.upv.dsic.gti_ia.core.ACLMessage;
import es.upv.dsic.gti_ia.core.SingleAgent;
import es.upv.dsic.gti_ia.core.AgentID;
import java.awt.Color;
import java.awt.Dimension;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author cristobal
 */
public class AgenteBot extends SingleAgent {

    //Variables usadas para la transmision de mensajes

    private AgentID Id, IdDatos, IdControlador;
    private String mundo;
    private String key;
    private ACLMessage inbox, outbox;

    //Variables que contienen las matrices
    private double[][] scanner;
    private int[][] radar;
    private int[][] mapa;
    private int x, y;
    private final int tam = 500;
    private double battery = 99.0;

    //GUI
    private Interfaz interfaz;

    //Para la hora en los mensajes
    Date date;
    DateFormat hourFormat;

    //###########################################
    //METODOS
    //###########################################
    public AgenteBot(AgentID Id, AgentID IdDatos, AgentID IdCont, String mundo) throws Exception {
        super(Id);
        outbox = new ACLMessage();
        this.Id = Id;
        this.mundo = mundo;
        this.IdControlador = IdCont;
        this.IdDatos = IdDatos;
        this.outbox = new ACLMessage();

        interfaz = new Interfaz();
        //Se inicializan los mapas
        scanner = new double[tam][tam];
        radar = new int[tam][tam];
        mapa = new int[tam][tam];
        //Se inicializa el mapa radar con obstaculos (1)
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                radar[i][j] = 1;
            }
        }
        //Para poner hora en los mensajes
        date = new Date();
        hourFormat = new SimpleDateFormat("HH:mm:ss");
    }

    /**
 *
 * @author Ruben
 */
    private void enviarMensaje(AgentID ID, JsonObject msgJson) {
        imprimirMensaje(" Enviando mensaje: " + msgJson.toString());
        //Se prepara y envia el mensaje
        outbox.setSender(this.getAid());
        outbox.setReceiver(ID);
        outbox.setContent(msgJson.toString());
        this.send(outbox);
    }

    private JsonObject recibirMensaje() throws Exception {
        //Espera una respuesta
        imprimirMensaje("Esperando mensaje");
        inbox = receiveACLMessage();
        if (inbox.getContent().startsWith("CRASHED")) {
            throw new Exception("Error: el bot ha chocado, suerte la proxima ;P");
        }
        imprimirMensaje(inbox.getContent());
        return JsonObject.readFrom(inbox.getContent());

    }

    private void logout() throws Exception {
        JsonObject mensajeJson = new JsonObject();
        mensajeJson.add("command", "logout");
        mensajeJson.add("key", key);
        enviarMensaje(IdControlador, mensajeJson);
    }

    private void imprimirMensaje(String msg) {
        System.out.println(hourFormat.format(date) + "-> " + this.getName() + ": " + msg);
    }

    //AQUI IRA EL CODIGO PARA LA HEURISTICA, DEVOLVERA "finalizado" SI RESULTA QUE HA LLEGADO A SU DESTINO PARA DETENER EL BUCLE
    //SE DECIDE QUE HACER, TAMBIEN TIENE EN CUENTA LA BATERIA
    //Para moverse move{N,S,E,W,NE,NW,SE,SW}
    //Para recargar refuel
/**
 *
 * @author Dario
 */
    private String getPaso() {
        
        if (radar[y][x] == 2) {
            return "finalizado";
        }
        if(battery<=5)
            return "refuel";
        //Esto es un intento de una heuristica busca en las celdas adyacentes la que
        //este libre y la mas cercana al objetivo, se queda dando vueltas en el mapa earthquake1
        int x_a=x-1,y_a=y+1;
        double dist=10000;
        /*
        X X X X X
        X 1 2 3 X
        X 4 B 5 X
        X 6 7 8 X
        X X X X X
        */
        for(int i=y-1;i<y+2;i++){
            for(int j=x-1;j<x+2;j++){
                //Si la celda esta libre se mira si nos movemos
                if(celdaLibre(j, i) && mapa[i][j]==0){
                    if(dist>scanner[i][j]){
                        dist=scanner[i][j];
                        x_a=j;
                        y_a=i;
                    }
                }
            }
        }
        //Esta funcion devuelve el movimiento que se corresponde con moverse a esta celda
        System.err.println("x="+(x_a-x)+"y="+(y_a-y)+"---"+mapa[y_a][x_a]);
        mapa[y_a][x_a]++;
        return direccion(x_a,y_a);
    }
    //Devuelve la direccion a la que deberia moverse dada la coordenada de una celda
    private String direccion(int x_a,int y_a){
        //Se obtiene un vector direccion
        int x_b=x_a-x;
        int y_b=y_a-y;
        //Se ira añadiendo a result la direccion que corresponda con el signo de x_b e y_b
        String result="move";
        if(y_b<0){
            result+="N";
        }
        else if(y_b>0){
            result+="S";
        }
        if(x_b<0){
            result+="W";
        }
        else if(x_b>0){
            result+="E";
        }
        return result;
    }
    //Devuelve true si se puede mover a la celda
    private boolean celdaLibre(int x_a,int y_a){
        //Si se sale de los limites
        if(x_a<0 || y_a<0 || x_a>=500 || y_a>=500)
            return false;
        if(x==x_a && y==y_a)
            return false;
        //Si no es la celda en la que esta el bot y si no es pared
        return (radar[y_a][x_a]!=1);
        
    }
    //##################################################################################################
    //##################################################################################################

    @Override/**
 *
 * @author Cristobal
 */
    public void execute() {
        interfaz.setVisible(true);
        try {
            JsonObject mensajeJson = new JsonObject();
            mensajeJson.add("command", "login");
            mensajeJson.add("world", mundo);
            mensajeJson.add("radar", IdDatos.name);
            mensajeJson.add("scanner", IdDatos.name);
            mensajeJson.add("battery", IdDatos.name);
            mensajeJson.add("gps", IdDatos.name);

            enviarMensaje(IdControlador, mensajeJson);

            //Espera una respuesta a la peticion de login
            mensajeJson = recibirMensaje();
            String re = mensajeJson.get("result").asString();

            if (re.startsWith("BAD")) {
                throw new Exception("Error: " + re);
            }
            System.out.println(hourFormat.format(date) + " - " + "Login correcto, key=" + re);

            //La key se usara para mandar las ordenes al controlador
            this.key = re;

            boolean continua = true;
            String sigPaso = null;
            while (continua) {
                //Si se ha decidido un paso se envia la orden
                if (sigPaso != null) {
                    mensajeJson = new JsonObject();
                    mensajeJson.add("command", sigPaso);
                    mensajeJson.add("key", this.key);
                    enviarMensaje(IdControlador, mensajeJson);
                    sigPaso = null;
                }
                //Aqui espera el mensaje de databot o el result de controlador
                mensajeJson = recibirMensaje();
                switch (mensajeJson.names().get(0)) {
                    case "percepcion":
                            //En esta parte se llamara a una funcion que decidira el siguiente paso y 
                        //se enviara la orden en la siguiente iteracion del bucle
                        //Primero las coordenadas
                        JsonObject gps = mensajeJson.get("gps").asObject();
                        x = gps.get("x").asInt();
                        y = gps.get("y").asInt();
                        interfaz.setGPS(x, y);
                        //Nivel de bateria
                        battery = mensajeJson.get("battery").asDouble();
                        interfaz.setBateria(battery);
                        
                        JsonArray radarJ = mensajeJson.get("radar").asArray();
                        JsonArray scannerJ = mensajeJson.get("scanner").asArray();
                        
                        //Se rellena el mapa de agenteBot se empezaran desde las coordenadas i2=x-2 y j2=y-2
                        //Las coordenadas de las matrices del mensaje(5x5) seran i y js
                        int i2 = y - 2,
                         j2 = x - 2;
                        System.out.print("-------------------------------------------------------");
                        for (int i = 0; i < 5 && i2 < tam; i++, i2++) {
                            System.out.print("\n");
                            for (int j = 0; j < 5 && j2 < tam; j++, j2++) {
                                if (i2 >= 0 && j2 >= 0) {
                                    radar[i2][j2] = radarJ.get(i * 5 + j).asInt();
                                    interfaz.setMapa(j2, i2, radar[i2][j2]);
                                    interfaz.setRadar(j,i , radar[i2][j2]);
                                    scanner[i2][j2] = scannerJ.get(i * 5 + j).asDouble();
                                    interfaz.setScanner(i,j,scanner[i2][j2]);
                                    System.out.print(radar[i2][j2]);
                                }
                            }
                            j2 -=5;
                        }
                        interfaz.pintarRecorrido();
                        System.out.print("\n");
                        sigPaso = getPaso();

                        /*
                         Si llega al objetivo con sigpaso="finalizado" (un 2 en sus coordenadas en el mapa del radar)
                         debera hacer un logout:
                         */
                        if ("finalizado".equals(sigPaso)) {
                            //Se termina el bucle
                            continua = false;

                            //finalizamos databot
                            mensajeJson = new JsonObject();
                            mensajeJson.add("muere", "muere");
                            enviarMensaje(IdDatos, mensajeJson);

                            //Mensaje de color verde dando por finalizado
                            System.out.print((char) 27 + "[0;32m");
                            System.out.println("#################################\n"
                                    + (char) 27 + "[0;32m" + "Ha llegado al objetivo!! x="+this.x+" y="+this.y+"\n"
                                    + (char) 27 + "[0;32m" + "#################################");
                            System.out.print((char) 27 + "[37m");
                            //Logout al controlador
                            logout();
                        }

                        break;
                    //Se guardara el estado de la bateria en una variable
                    case "result":
                        //Aqui se lanza una excepcion si ha fallado el movimiento
                        re = mensajeJson.get("result").asString();
                        if (re.startsWith("BAD")) {
                            throw new Exception("Error: " + re);
                        }
                        if (re.startsWith("CRASHED")) {
                            throw new Exception("Error: el bot ha chocado, suerte la proxima ;P");
                        }
                }
            }
        } catch (Exception e) {
            //e.printStackTrace(System.out);
            //int lineNumber=e.getStackTrace()[0].getLineNumber();
            System.err.println("##################\nError en " + this.getName() + "->" + e.getMessage());
            e.printStackTrace(System.out);
        }
    }
}
