/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import es.upv.dsic.gti_ia.core.AgentID;
import es.upv.dsic.gti_ia.core.AgentsConnection;

/**
 *
 * @author cristobal
 */
public class Practica2 {
    
    public static void main(String[] args) throws Exception{
        AgentsConnection.connect("siadex.ugr.es", 6000, "Andromeda", "Aries", "Picasso", false);
        //Aqui creamos las ids de los agentes que intervienen
        AgentID IdBot=new AgentID("bot222");
        AgentID IdDatos=new AgentID("datos222");
        AgentID IdControlador=new AgentID("Andromeda");
        
        AgenteBot agenteBot=new AgenteBot(IdBot,IdDatos,IdControlador,"earthquake1");
        DataBot agenteDatos=new DataBot(IdDatos,IdBot);
         agenteDatos.start();
        agenteBot.start();
       
        
    }
  
}
