package practica2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import java.util.ArrayList;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Jose Miguel
 */
public class Interfaz extends javax.swing.JFrame {

    /**
     * Creates new form Prueba
     */
    
    private javax.swing.JLabel nombreBateria;
    private javax.swing.JLabel nombreGPS;
    private javax.swing.JLabel nombreX;
    private javax.swing.JLabel nombreY;
    private javax.swing.JLabel valorX;
    private javax.swing.JLabel valorY;
    private javax.swing.JLabel nombreScanner;
    private javax.swing.JLabel nombreRadar;
    private Mapa mapa;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JPanel panelDatosScanner;
    private Mapa radar;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane panelScroll;
    private javax.swing.JSeparator jSeparator1;
    private ArrayList<JLabel> valorScanner;
    // Variables declaration - do not modify                     

    public Interfaz(){
        initComponents();
        this.setMinimumSize(new Dimension(1150, 500));
        this.setMaximumSize(new Dimension(1150, 600));
        this.setTitle("Practica 2 DBA");
    }
    
    private void initComponents() {

        panelScroll = new javax.swing.JScrollPane();
        mapa = new Mapa();
        panelDatos = new javax.swing.JPanel();
        jProgressBar1 = new javax.swing.JProgressBar();
        nombreBateria = new javax.swing.JLabel();
        nombreGPS = new javax.swing.JLabel();
        nombreX = new javax.swing.JLabel();
        nombreY = new javax.swing.JLabel();
        valorX = new javax.swing.JLabel();
        valorY = new javax.swing.JLabel();
        nombreScanner = new javax.swing.JLabel();
        nombreRadar = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        panelDatosScanner = new javax.swing.JPanel();
        valorScanner = new ArrayList<JLabel>();
        radar = new Mapa(100,100,20,20);

        for (int i = 0; i < 25; i++) {
           
            valorScanner.add(new JLabel());
        }
     
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(mapa);
        mapa.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 447, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 565, Short.MAX_VALUE)
        );
        
        jProgressBar1.setBackground(Color.white);
        jProgressBar1.setBorder(new LineBorder(Color.black));
        panelScroll.setViewportView(mapa);

        nombreBateria.setText("Bateria");

        nombreGPS.setText("GPS");

        nombreX.setText("X");

        nombreY.setText("Y");

        valorX.setText("valorX");

        valorY.setText("valorY");

        nombreScanner.setText("Scanner");

        valorScanner.get(0).setText("0");

        valorScanner.get(1).setText("0");

        valorScanner.get(2).setText("0");

        valorScanner.get(3).setText("0");

        valorScanner.get(4).setText("0");

        valorScanner.get(5).setText("0");

        valorScanner.get(6).setText("0");

        valorScanner.get(7).setText("0");

        valorScanner.get(8).setText("0");

        valorScanner.get(9).setText("0");

        valorScanner.get(10).setText("0");

        valorScanner.get(11).setText("0");

        valorScanner.get(12).setText("0");

        valorScanner.get(13).setText("0");

        valorScanner.get(14).setText("0");

        valorScanner.get(15).setText("0");

        valorScanner.get(16).setText("0");

        valorScanner.get(17).setText("0");

        valorScanner.get(18).setText("0");

        valorScanner.get(19).setText("0");

        valorScanner.get(20).setText("0");

        valorScanner.get(21).setText("0");

        valorScanner.get(22).setText("0");

        valorScanner.get(23).setText("0");

        valorScanner.get(24).setText("0");

        nombreRadar.setText("Radar");
        
        radar.setBorder(new LineBorder(Color.black, 2));
        
        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(panelDatosScanner);
        panelDatosScanner.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(0))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(valorScanner.get(1)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(6))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(9))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(4)))))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(14)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(16))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(19)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(valorScanner.get(20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(22))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valorScanner.get(24))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorScanner.get(0))
                    .addComponent(valorScanner.get(1))
                    .addComponent(valorScanner.get(2))
                    .addComponent(valorScanner.get(3))
                    .addComponent(valorScanner.get(4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorScanner.get(5))
                    .addComponent(valorScanner.get(6))
                    .addComponent(valorScanner.get(7))
                    .addComponent(valorScanner.get(8))
                    .addComponent(valorScanner.get(9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorScanner.get(10))
                    .addComponent(valorScanner.get(11))
                    .addComponent(valorScanner.get(12))
                    .addComponent(valorScanner.get(13))
                    .addComponent(valorScanner.get(14)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorScanner.get(15))
                    .addComponent(valorScanner.get(16))
                    .addComponent(valorScanner.get(17))
                    .addComponent(valorScanner.get(18))
                    .addComponent(valorScanner.get(19)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorScanner.get(20))
                    .addComponent(valorScanner.get(21))
                    .addComponent(valorScanner.get(22))
                    .addComponent(valorScanner.get(23))
                    .addComponent(valorScanner.get(24)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(radar);
        radar.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 160, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 158, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelDatosScanner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nombreBateria)
                                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(nombreGPS)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(nombreX)
                                            .addComponent(valorX))
                                        .addGap(26, 26, 26)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(valorY)
                                            .addComponent(nombreY)))))
                            .addComponent(nombreScanner)
                            .addComponent(nombreRadar)
                            .addComponent(radar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreBateria)
                .addGap(18, 18, 18)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreGPS)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreX)
                    .addComponent(nombreY))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorX)
                    .addComponent(valorY))
                .addGap(22, 22, 22)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nombreScanner)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelDatosScanner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nombreRadar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panelScroll)))
                .addContainerGap())
        );

        pack();
    }
    public void setGPS(int x, int y){
        
        valorX.setText(""+x);
        valorY.setText(""+y);
        //Se guarda el recorrido que ha hecho
        mapa.recorrido.add(new int[]{x,y});
        //Se mueven los scrollbars a para poner el bot en el centro
        int X=x*10-350;
        int Y=y*10-350;
        try {
            JScrollBar v=panelScroll.getVerticalScrollBar();
        JScrollBar h=panelScroll.getHorizontalScrollBar();
        if(Y>0 && panelScroll.getVerticalScrollBar()!=null)v.setValue(Y);
        if(X>0 && panelScroll.getHorizontalScrollBar()!=null)h.setValue(X);
        //repaint();
        } catch (Exception e) {
        }
        
    }
    
    public void setBateria(double valor){
        
        jProgressBar1.setValue((int)valor);
        if(valor > 50){
            jProgressBar1.setForeground(Color.green);
        }
        else
            if(valor>10){
                jProgressBar1.setForeground(Color.yellow);
            }
            else{
                jProgressBar1.setForeground(Color.red);
            }
        
    }
    
    public void setMapa(int i, int j, int tipo){
        mapa.setValor(i, j, tipo);
    }
    public void setRadar(int i, int j, int tipo){
        radar.setValor(i, j, tipo);
    }
    
    public void pintarPunto(int x, int y){
        setMapa(x,y,3);
    }

    public void pintarRecorrido() {
       mapa.pintarRecorrido();
    }
    
    public void setScanner(int i,int j,double s){

        valorScanner.get((i*5)+j).setText(""+s);

    }
}

