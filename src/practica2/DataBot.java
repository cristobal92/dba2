/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import es.upv.dsic.gti_ia.core.ACLMessage;
import es.upv.dsic.gti_ia.core.AgentID;
import es.upv.dsic.gti_ia.core.SingleAgent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Jose Miguel
 */
public class DataBot extends SingleAgent{ 
    
    public double battery = 0.0;
    public JsonArray radar;
    public boolean bradar=false, bscaner=false, bbateria=false, bgps=false, bresult=false, continuar=true;
    public ArrayList<Double> scanner;
    private ACLMessage inbox,outbox;
    private AgentID agentebot;
    //Para la hora en los mensajes
    Date date;
    DateFormat hourFormat;
    

    public DataBot(AgentID aid, AgentID boto) throws Exception {
        super(aid);
        agentebot = boto;
        this.radar = new JsonArray();
        inbox=new ACLMessage();
        outbox=new ACLMessage();
        
        date = new Date();
        hourFormat=new SimpleDateFormat("HH:mm:ss");
    }
    
    /**
 *
 * @author Jorge
 */
     private void enviarMensaje(AgentID ID,JsonObject msgJson){
        imprimirMensaje("Enviando mensaje: "+msgJson.toString());
        //Se prepara y envia el mensaje
        outbox.setSender(this.getAid());
        outbox.setReceiver(this.agentebot);
        outbox.setContent(msgJson.toString());
        this.send(outbox);
         
    }
    
    private JsonObject recibirMensaje() throws Exception{
        //Espera una respuesta
        imprimirMensaje("esperando mensaje");
        inbox=receiveACLMessage();
        if(inbox.getContent().startsWith("CRASHED"))
            throw new Exception("Error: el bot ha chocado, suerte la proxima ;P");
        //System.err.println("----->DataBotRecibido mensaje"+inbox.getContent());
        return JsonObject.readFrom(inbox.getContent());
        
    }
    private void imprimirMensaje(String msg){
        System.out.println(hourFormat.format(date)+"-> "+this.getName()+": "+msg);
    }
    @Override
    
    /**
 *
 * @author Carlos
 */
    public void execute(){
        try {
            ACLMessage inbox;
            JsonObject mensaje;
            JsonArray fsc = new JsonArray();
            JsonArray fra = new JsonArray();
            JsonObject fgps = new JsonObject();
            float fbat =0;
        while(continuar){
            //Se envian los datos en un unico mensaje
            if (bbateria && bradar && bscaner && bgps) {
                JsonObject mensajeJson = new JsonObject();
                mensajeJson.add("percepcion", "Datos");
                mensajeJson.add("scanner", fsc);
                mensajeJson.add("radar", fra);
                mensajeJson.add("gps", fgps);
                mensajeJson.add("battery", fbat);
                enviarMensaje(agentebot, mensajeJson);
                bbateria = false;
                bgps = false;
                bradar = false;
                bscaner = false;

            }
            mensaje=recibirMensaje();

            String opcion=mensaje.names().get(0);
            //System.out.println(hourFormat.format(date)+" - "+mensaje.names().get(0));
            //Aqui se tiene que captar los mensajes segun el primer nombre del json se decide si corresponde a gps, rdar etc
            switch(opcion){
                case "scanner":
                    fsc=mensaje.get("scanner").asArray(); 
                    bscaner=true;
                    break;
                case "radar":
                    fra=mensaje.get("radar").asArray();
                    bradar=true;
                    break;
                case "gps":
                    
                    fgps=mensaje.get("gps").asObject();
                    bgps = true;
                    break;
                case "battery":
                    fbat=mensaje.get("battery").asFloat();
                    bbateria = true;
                    break;
                case "muere":
                    imprimirMensaje("Parado por agentebot");
                    continuar=false;
            }
        }   
            
        }catch (Exception e) {
            //e.printStackTrace(System.out);
            System.err.println("##################\nError en "+this.getName()+"->"+e.getMessage());
        }
        
    }
}